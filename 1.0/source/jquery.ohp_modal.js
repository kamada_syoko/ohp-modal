(function($) {
    $.fn.ohp_modal = function(options){
        var defaults = {
            easing : 'linear',
            speed  : 500,
            group  : true,
            loop   : false,
            zIndex: 10000
        };
        var setting = $.extend(defaults, options);

//-------------------------------

		var self = $(this);
        var id = '';

        $('body').append('<div id="ohp-modal-overlay"></div>');
        $('.modal-content > *').wrapAll('<div id="ohp-modal-content">');
        $('.modal-content').css('z-index', setting.zIndex).append('<div id="ohp-modal-nav"><div id="ohp-modal-close"><span></span><span></span></div></div>');
        $('#ohp-modal-overlay').css('z-index', setting.zIndex - 1);

        if ( setting.group == true ){
            $('#ohp-modal-nav').append('<a class="slider-prev" href="" aria-label="Previous">&lt;</a><a class="slider-next" href="" aria-label="Next">&gt;</a>');

    		// ウインドウ数を取得
    		var windowCount = $('#ohp-modal-content > *').length;
        }

//-------------------------------

        $('a.modal').on('click', function(){
            id = $(this).attr('href');
            openModal(id);

            return false;
        });

//-------------------------------

        $('#ohp-modal-overlay, #ohp-modal-close').on('click', function(){
            closeModal();
        });

//-------------------------------

        $('#ohp-modal-nav .slider-prev, #ohp-modal-nav .slider-next').on('click', function(){
            id = $(this).attr('href');
            slideModal(id);

            return false;
        });

//-------------------------------

        $(window).on('resize', function(){
            setModalScroll(id);
        });

//-------------------------------

        function openModal(id){
            $('body, html').css('overflow', 'hidden');
            $('.modal-content').animate({opacity: 'show'},
                {
                    duration: setting.speed,
                    easing: setting.easing
                }
            );
            $(id).fadeIn();
            $('#ohp-modal-content, #ohp-modal-overlay, #ohp-modal-nav').fadeIn();

            setModalNav(id);
        }

//-------------------------------

        function closeModal(){
            $('body, html').css('overflow', 'auto');
            $('.modal-content').animate({opacity: 'hide'},
                {
                    duration: setting.speed,
                    easing: setting.easing
                }
            );
            $('#ohp-modal-content > *').fadeOut();
            $('#ohp-modal-overlay').fadeOut();
            $('#ohp-modal-nav > div').not('#ohp-modal-close').fadeOut();
        }

//-------------------------------

        function slideModal(id){
            $.when(
                $('#ohp-modal-content > *').fadeOut(setting.speed)
            ).done(function(){
                $('#ohp-modal-nav > a').hide();
                setModalNav(id);
                $(id).fadeIn(setting.speed);
            });
        }

//-------------------------------

        function setModalNav(id){
            setModalScroll(id);

            // Prev Next
            if ( setting.group == true && windowCount > 1 ){
                var elm   = $(id);
                var index = $('#ohp-modal-content > *').index(elm);

                if ( index > 0 ){
                    $('#ohp-modal-nav .slider-prev').show().attr('href', '#' + $('#ohp-modal-content > *:eq('+(index - 1)+')').attr('id'));
                }
                if ( index < windowCount - 1 ){
                    $('#ohp-modal-nav .slider-next').show().attr('href', '#' + $('#ohp-modal-content > *:eq('+(index + 1)+')').attr('id'));
                }
            }
        }

//-------------------------------

        function setModalScroll(id){
            if ( $(id).height() > $('#ohp-modal-content').height() ){
                $('#ohp-modal-content').css('overflow-y', 'scroll');
            }else{
                $('#ohp-modal-content').css('overflow-y', 'hidden');
            }
        }

//-------------------------------

        return this;
    };
})(jQuery);