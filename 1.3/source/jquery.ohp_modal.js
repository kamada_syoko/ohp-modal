(function($) {
    $.fn.ohp_modal = function(options){
        var defaults = {
            easing : 'linear',
            speed  : 500,
            group  : true,
            loop   : false,
            zIndex: 10000
        };
        var setting = $.extend(defaults, options);

//-------------------------------

		var self = $(this);
        var id = '';

        $('body').append('<div id="ohp-modal-overlay"></div>');
        $('.modal-content > *').wrapAll('<div id="ohp-modal-content">');
        $('.modal-content').css('z-index', setting.zIndex).append('<div id="ohp-modal-nav"><button id="ohp-modal-close" tabindex="0">Close<span></span><span></span></button></div>');
        $('#ohp-modal-overlay').css('z-index', setting.zIndex - 1);
        $('#ohp-modal-content > *').attr('role', 'dialog').attr('aria-live', 'polite');

        if ( setting.group == true ){
            $('#ohp-modal-nav').append('<a class="slider-prev" href="" aria-label="Previous">&lt;</a><a class="slider-next" href="" aria-label="Next">&gt;</a>');

    		// ウインドウ数を取得
    		var windowCount = $('#ohp-modal-content > *').length;
        }

//-------------------------------

        $('a.modal').on('click', function(){
            id = '#' + $(this).attr('href').split('#')[1];
            openModal();

            return false;
        });

//-------------------------------

        $('#ohp-modal-overlay, #ohp-modal-close, .modal-close').on('click', function(){
            closeModal();
        });

        $(window).keyup(function(e){
        	if ( e.keyCode == 27 ){   // ESCキー
                closeModal();
            }
        });

//-------------------------------

        $('#ohp-modal-nav .slider-prev, #ohp-modal-nav .slider-next').on('click', function(){
            id = '#' + $(this).attr('href').split('#')[1];
            slideModal();

            return false;
        });

//-------------------------------

        $(window).on('resize', function(){
            setModalScroll();
        });

//-------------------------------

        $('html, body, body *').not('.modal-content *').on('focus', function(){
            if ( $('body').hasClass('ohp-modal-open') ){
                $('#ohp-modal-close').focus();
            }
        });

//-------------------------------

        function openModal(){
            $('body').addClass('ohp-modal-open');
            $('body, html').css('overflow', 'hidden');
            $('.modal-content').animate({opacity: 'show'},
                {
                    duration: setting.speed,
                    easing: setting.easing
                }
            );
            $(id).fadeIn();
            $('#ohp-modal-content, #ohp-modal-overlay, #ohp-modal-nav').fadeIn();

            setModalNav();
        }

//-------------------------------

        function closeModal(){
            $('body').removeClass('ohp-modal-open');
            $('body, html').css('overflow', 'auto');
            $('.modal-content').animate({opacity: 'hide'},
                {
                    duration: setting.speed,
                    easing: setting.easing
                }
            );
            $('#ohp-modal-content > *').fadeOut();
            $('#ohp-modal-overlay').fadeOut();
            $('#ohp-modal-nav > div').not('#ohp-modal-close').fadeOut();
            $('a[href="'+id+'"]').focus();
        }

//-------------------------------

        function slideModal(){
            $('body').addClass('ohp-modal-open');

            $.when(
                $('#ohp-modal-content > *').fadeOut(setting.speed)
            ).done(function(){
                $('#ohp-modal-nav > a').hide();
                setModalNav(id);
                $(id).fadeIn(setting.speed);
            });
        }

//-------------------------------

        function setModalNav(){
            setModalScroll(id);

            // WAI-ARIA
            $('#ohp-modal-content > *').children('*:nth-child(1)').attr('id', '');
            $('#ohp-modal-content > *').children('*:nth-child(2)').attr('id', '');
            $(id).children('*:nth-child(1)').attr('id', 'ohp-modal-title');
            $(id).children('*:nth-child(2)').attr('id', 'ohp-modal-describ');
            $(id).attr('aria-labelledby', $(id).children('*:nth-child(1)').attr('id')).attr('aria-describedby', $(id).children('*:nth-child(2)').attr('id'));
            $('#ohp-modal-close').focus();

            // Prev Next
            if ( setting.group == true && windowCount > 1 ){
                var elm   = $(id);
                var index = $('#ohp-modal-content > *').index(elm);

                if ( index > 0 ){
                    $('#ohp-modal-nav .slider-prev').show().attr('href', '#' + $('#ohp-modal-content > *:eq('+(index - 1)+')').attr('id'));
                }else if ( setting.loop == true ){
                    $('#ohp-modal-nav .slider-prev').show().attr('href', '#' + $('#ohp-modal-content > *:eq('+(windowCount - 1)+')').attr('id'));
                }
                if ( index < windowCount - 1 ){
                    $('#ohp-modal-nav .slider-next').show().attr('href', '#' + $('#ohp-modal-content > *:eq('+(index + 1)+')').attr('id'));
                }else if ( setting.loop == true ){
                    $('#ohp-modal-nav .slider-next').show().attr('href', '#' + $('#ohp-modal-content > *:eq(0)').attr('id'));
                }
            }
        }

//-------------------------------

        function setModalScroll(){
            if ( $(id).height() > $('#ohp-modal-content').height() ){
                $('#ohp-modal-content').css('overflow-y', 'scroll');
            }else{
                $('#ohp-modal-content').css('overflow-y', 'hidden');
            }
        }

//-------------------------------

        return this;
    };
})(jQuery);